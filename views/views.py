import sys
from socket import *

from PyQt4.QtGui import *

from controller.controle import *


# Camada View
class View(QWidget):
  def __init__(self):
    self.ctrl = Controle()
    self.app_ = QApplication(sys.argv)

    QWidget.__init__(self)

    self.confiGUI()

  def confiGUI(self):
    # Saida de Texto
    self.txtDisplay = QTextEdit()

    self.txtDisplay.setFixedHeight(420)

    # Comandos
    self.bntScaner = QPushButton("Scanear Portas")
    self.bntScaner.setFixedWidth(150)
    self.bntScaner.clicked.connect(self.calcularRede)

    # Entrada de Dados
    self.inputIP = QLineEdit()

    self.inputMask = QLineEdit()

    # Organizando Layout

    self.inputIP

    self.inputMask
    self.txtDisplay

    # Formulario


    hbox = QHBoxLayout()
    vbox1 = QVBoxLayout()
    vbox2 = QVBoxLayout()

    vbox1.addWidget(QLabel("Infome o IP de Rede:"))
    vbox1.addWidget(self.inputIP)
    vbox1.addWidget(QLabel("Mascara de Rede:"))
    vbox1.addWidget(self.inputMask)
    vbox1.addWidget(self.bntScaner)
    vbox1.addStretch()

    # Saida
    vbox2.addWidget(QLabel("Saida do Processamento:"))

    lista = self.ctrl.getListaDeIPs()
    texto = ""

    for saida in lista:
      texto = texto + saida
      self.txtDisplay.setText(texto)

    vbox2.addWidget(self.txtDisplay)
    vbox2.addStretch()

    hbox.addLayout(vbox1)
    hbox.addLayout(vbox2)

    self.setLayout(hbox)
    self.setWindowTitle("Hackeando a UNIRN")
    self.setGeometry(200, 200, 400, 500)
    self.setFixedHeight(500)
    self.show()
    sys.exit(self.app_.exec_())

  def calcularRede(self):
    self.ctrl.calcularRede(self.inputIP.text(), self.inputMask.text())
