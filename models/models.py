# Camada de Modelo

# !/usr/bin/env python
# -*- encoding: utf-8 -*-

"""@package docstring
	Author: UNIRN, Daniel Rodrigues,Andre Braz

 Esse script recebe do usuário duas entradas:
 - IP de rede
 - Máscara de rede

 E imprime a lista de todos os IPs válidos nessa rede.
"""

from netaddr import *


class Modelo(object):
  maquinas = []

  def getListaDeIPs(self):
    return self.maquinas

  def calcularRedes(self, ipRede, maskRede):
    self.netaddr = ipRede[:15]  # endereço IP de rede
    self.netmask = maskRede.strip('/')[:15]  # máscara /24 ou 255.255.255.0

    self.network = IPNetwork(self.netaddr + '/' + self.netmask)

    self.maquinas.append(self.network)
